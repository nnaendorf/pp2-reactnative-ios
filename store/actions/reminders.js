export const fetchReminders = session => {
    return async dispatch => {
        const response = await fetch(
            'http://212.227.10.211:8080/pp-app-server/reminder',
            {
                method: 'GET',
                headers: {
                    'cookie': session,
                }
            }
        )
        const resData = await response.json();
        dispatch({type: 'FETCH', reminders: resData})
    }
}
