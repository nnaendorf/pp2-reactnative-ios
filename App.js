import React from 'react';
import VaterNavigator from './navigation/VaterNavigator';
import { enableScreens } from 'react-native-screens';
import { combineReducers, createStore, applyMiddleware } from 'redux';
import authReducer from './store/reducers/auth';
import { Provider } from 'react-redux';
import thunk from 'redux-thunk';
import remindersReducer from './store/reducers/reminders';

enableScreens();

const rootReducer = combineReducers({
  auth: authReducer,
  reminders: remindersReducer,
});

const store = createStore(rootReducer, applyMiddleware(thunk));

export default function App() {
  return (
    <Provider store={store}>
      <VaterNavigator/>
    </Provider>
  );
}

