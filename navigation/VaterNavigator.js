import { createStackNavigator } from 'react-navigation-stack';
import { createAppContainer, createSwitchNavigator } from 'react-navigation';

import Colors from "../constants/Colors";
import HomeScreen from '../screens/HomeScreen';
import InvoiceScreen from '../screens/InvoiceScreen';
import LicenseScreen from '../screens/LicenseScreen';
import ReminderScreen from '../screens/ReminderScreen';
import AuthScreen from '../screens/AuthScreen';
import { createDrawerNavigator } from 'react-navigation-drawer';
import NewReminderScreen from '../screens/NewReminderScreen';

const defaultStackNavOptions = {
    headerStyle: {
        backgroundColor: Colors.primaryColor,
    },
    headerTintColor: 'white',
}

const VaterStackNavigator = createStackNavigator({
    Home: HomeScreen,
    Invoice: InvoiceScreen,
    License: LicenseScreen,
    Reminder: ReminderScreen,
    NewReminder: NewReminderScreen,
    }, {
    defaultNavigationOptions: defaultStackNavOptions,
    }
);

const VaterAuthNavigator = createStackNavigator({
    Auth: AuthScreen,
    }, {
        defaultNavigationOptions: defaultStackNavOptions,
    }
);

const VaterDrawerNavigator = createDrawerNavigator({
    Home: {
        screen: VaterStackNavigator,
        navigationOptions: {
            drawerLabel: 'Übersicht',
        }
    },
    Invoice: {
        screen: InvoiceScreen,
        navigationOptions: {
            drawerLabel: 'Belege',
        }
    },
    License: {
        screen: LicenseScreen,
        navigationOptions: {
            drawerLabel: 'Führerschein',
        }
    },
    Reminder: {
        screen: ReminderScreen,
        navigationOptions: {
            drawerLabel: 'Erinnerungen',
        }
    },
    Auth: {
        screen: AuthScreen,
        navigationOptions: {
            drawerLabel: 'Logout'
        }
    }
});

const MainNavigator = createSwitchNavigator({
    Auth: VaterAuthNavigator,
    Home: VaterDrawerNavigator,
});

export default createAppContainer(MainNavigator);