import React from 'react';
import { View, Text, StyleSheet } from 'react-native';

const ListItem = props => {
    return <View style={styles.listItem}>
        <Text>{props.children}</Text><Text style={styles.clock}>{props.time}</Text>
    </View>
}

const styles = StyleSheet.create({
    listItem: {
        marginVertical: 5,
        marginHorizontal: 30,
        borderWidth: 2,
        borderColor: '#dedede',
        padding: 10,
        flexDirection: 'row',
    }, 
    clock: {
        marginLeft: 'auto'
    },
});

export default ListItem;